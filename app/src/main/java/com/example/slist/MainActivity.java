package com.example.slist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.slist.activity.DashboardActivity;

public class MainActivity extends AppCompatActivity {

    private static final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                
                /** 
                 * Create an Intent that will start 
                 * the Dashboard activity after 3 second
                 **/
                startActivity( new Intent(MainActivity.this, DashboardActivity.class));
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }

}
