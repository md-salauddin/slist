package com.example.slist.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.slist.R;
import com.example.slist.model.Product;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AdminAddingProduct extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private EditText productName;
    private Spinner language, bn_category, en_category;
    private Button add;

    private int langCode;
    private String categoryName;

    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_adding_product);

        productName = findViewById(R.id.add_product_edt);
        language = findViewById(R.id.language_spinner);
        bn_category = findViewById(R.id.bn_category_spinner);
        en_category = findViewById(R.id.en_category_spinner);
        add = findViewById(R.id.add_btn);

        databaseReference =  FirebaseDatabase.getInstance().getReference("slist");

        add.setOnClickListener(this);
        language.setOnItemSelectedListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.add_btn:
                saveData();
                break;

        }

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        langCode = language.getSelectedItemPosition();

        if (langCode == 0) {
            en_category.setVisibility(View.GONE);
            bn_category.setVisibility(View.VISIBLE);
        }
        else if (langCode == 1) {
            bn_category.setVisibility(View.GONE);
            en_category.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void saveData() {
        String name = productName.getText().toString();
        String productId = databaseReference.push().getKey();
        String categoryId = databaseReference.push().getKey();

        if (! TextUtils.isEmpty(name)) {
            Product product = new Product(productId, name);

            if (langCode == 0)
                categoryName = bn_category.getSelectedItem().toString();
            else if (langCode == 1)
                categoryName = en_category.getSelectedItem().toString();

            databaseReference.child(String.valueOf(langCode))
                    .child(categoryName)
                    .child(categoryId)
                    .setValue(product);

            productName.setText("");

            Toast.makeText(getApplicationContext(),
                    getString(R.string.success_message),
                    Toast.LENGTH_SHORT).show();
        }
        else
            productName.setError(getString(R.string.product_empty_error));
    }


}
