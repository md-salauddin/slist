package com.example.slist.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.slist.R;
import com.example.slist.fragment.Store;
import com.example.slist.fragment.TodayList;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DashboardActivity extends AppCompatActivity
        implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private AlertDialog alertDialog;
    private Spinner spinner;
    private Button okBtn, next;

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        bottomNavigationView = findViewById(R.id.bottom_navigation_dashboard);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        loadFragment(new TodayList());

        // admin access only
        //next = findViewById(R.id.next_btn);
        //next.setOnClickListener(this);
        //showCustomDialog();



    }


    private void showCustomDialog() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.
                from(this).
                inflate(R.layout.dialog_box_confirm_product, viewGroup, false);


        spinner = dialogView.findViewById(R.id.dialog_confirm_language_spinner);
        okBtn = dialogView.findViewById(R.id.dialog_confirm_ok_btn);

        okBtn.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);

        alertDialog = builder.create();
        alertDialog.show();
        //alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fg_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.dialog_confirm_ok_btn:
                int langCode = spinner.getSelectedItemPosition();
                Toast.makeText(getApplicationContext(), "Language code : "+langCode, Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
                break;

//            case R.id.next_btn:
//                startActivity(new Intent(this, AdminAddingProduct.class));
//                break;

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.nav_today_list:
                fragment = new TodayList();
                break;

            case R.id.nav_store:
                fragment = new Store();
                break;

            case R.id.nav_records:
                break;

        }

        return loadFragment(fragment);
    }


}
