package com.example.slist.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.slist.R;
import com.example.slist.adapter.TodayListAdapter;
import com.example.slist.interfacce.RvTodayList;
import com.example.slist.model.ShoppingList;

import java.util.ArrayList;


public class TodayList extends Fragment implements RvTodayList {

    private RecyclerView recyclerView;
    private ArrayList<ShoppingList> shoppingLists;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view;
        view = inflater.inflate(R.layout.fg_todays_list, container, false);

        shoppingLists = new ArrayList<>();
        shoppingLists.add(new ShoppingList("Birthday", "Cake, Candle, Apple ..."));
        shoppingLists.add(new ShoppingList("Grand dinner", "Chicken, Rice, Coke ..."));
        shoppingLists.add(new ShoppingList("Baby's shopping", "Daiper, Loshan ..."));
        shoppingLists.add(new ShoppingList("For lunch", "Potato, Fish, Egg ..."));

        TodayListAdapter todayListAdapter = new TodayListAdapter(getContext(), shoppingLists, this);
        recyclerView = view.findViewById(R.id.rv_today_list);
        recyclerView.setAdapter(todayListAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);


        return view;

    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(getContext(), shoppingLists.get(position).getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemLongClick(int position) {

    }

    private void staggeredGridRecyclerView(){
        TodayListAdapter todayListAdapter = new TodayListAdapter(getContext(), shoppingLists, this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(todayListAdapter);
    }


}
