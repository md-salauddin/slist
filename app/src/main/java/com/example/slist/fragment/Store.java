package com.example.slist.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.slist.R;
import com.example.slist.adapter.StoreParentAdapter;
import com.example.slist.model.Category;
import com.example.slist.model.Product;

import java.util.ArrayList;
import java.util.List;

public class Store extends Fragment {

    private RecyclerView recyclerView;
    private StoreParentAdapter storeParentAdapter;
    private List<Category> categories;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fg_store, container, false);

        createCategory();

        recyclerView = view.findViewById(R.id.rv_store);
        storeParentAdapter = new StoreParentAdapter(getContext(), categories);
        recyclerView.setAdapter(storeParentAdapter);

        //recyclerView.setNestedScrollingEnabled(false);

        return view;
    }


    public void createCategory() {
        categories = new ArrayList<>();
        List<Product> products1 = new ArrayList<>();
        List<Product> products2 = new ArrayList<>();
        List<Product> products3 = new ArrayList<>();

        products1.add(new Product("1", "Potato"));
        products1.add(new Product("2", "Banana"));
        products1.add(new Product("3", "Asparagus"));
        products1.add(new Product("4", "Arrowroot"));
        products1.add(new Product("5", "Azuki Beans"));

        products2.add(new Product("1", "Apple"));
        products2.add(new Product("2", "Banana"));
        products2.add(new Product("3", "Orange"));
        products2.add(new Product("4", "Grave"));
        products2.add(new Product("5", "Green apple"));

        products3.add(new Product("1", "Milk"));
        products3.add(new Product("2", "Banana Juice"));
        products3.add(new Product("3", "Soft drinks"));

        categories.add(new Category("1","Vegetable","#ff7979", products1));
        categories.add(new Category("2","Fruits","#2cb1e1", products2));
        categories.add(new Category("3","Drinks","#c182e0", products3));

    }

}
