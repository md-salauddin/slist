package com.example.slist.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.slist.model.Category;

import java.util.List;

public class CategoryViewModel extends ViewModel {

    private MutableLiveData<List<Category>> categories;

    public LiveData<List<Category>> getCategories() {

        if (categories == null) {
            categories = new MutableLiveData<>();
            loadCategories();
        }

        return categories;
    }

    private void loadCategories() {
    }


}
