package com.example.slist.model;

public class ShoppingList {

    private String name;
    private String shopHint;

    public ShoppingList(String name, String shopHint) {
        this.name = name;
        this.shopHint = shopHint;
    }

    public String getName() {
        return name;
    }

    public String getShopHint() {
        return shopHint;
    }

}
