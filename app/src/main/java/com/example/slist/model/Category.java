package com.example.slist.model;

import java.util.List;

public class Category {

    private String id;
    private String name;
    private String colorCode;
    private List<Product> products;

    public Category(String id, List<Product> products) {
        this.id = id;
        this.products = products;
    }

    public Category(String id, String name, String colorCode, List<Product> products) {
        this.id = id;
        this.name = name;
        this.colorCode = colorCode;
        this.products = products;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColorCode() {
        return colorCode;
    }

    public List<Product> getProducts() {
        return products;
    }

}
