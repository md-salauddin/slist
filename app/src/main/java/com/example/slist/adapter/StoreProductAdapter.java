package com.example.slist.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.slist.R;
import com.example.slist.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StoreProductAdapter extends RecyclerView.Adapter<StoreProductAdapter.ViewHolder>{

    private Context context;
    private List<Product> products;
    private int categoryPosition;
    private String colorCode;

    public StoreProductAdapter(Context context, List<Product> products, int categoryPosition, String colorCode) {
        this.context = context;
        this.products = products;
        this.categoryPosition = categoryPosition;
        this.colorCode = colorCode;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.cs_store_product_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = products.get(position);

        holder.productName.setText(product.getName());
        holder.firstLetter.setText(product.getName().substring(0, 1));
        holder.rlColorBar.setBackgroundColor(Color.parseColor(colorCode));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public String randomColor () {
        List<String> color = new ArrayList<>();

        color.add("#ff7979");
        color.add("#2cb1e1");
        color.add("#c182e0");
        color.add("#92c500");
        color.add("#f83a3a");

        Random rand = new Random();
        int x = rand.nextInt(4);

        return  color.get(x);

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView productName, firstLetter;
        RelativeLayout rlColorBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            productName = itemView.findViewById(R.id.rv_store_product_name);
            firstLetter = itemView.findViewById(R.id.rv_store_product_first_letter);
            rlColorBar = itemView.findViewById(R.id.rv_store_product_color_bar);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, categoryPosition+"-"+products.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
        }

    }

}
