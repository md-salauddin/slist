package com.example.slist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.slist.R;
import com.example.slist.model.Category;
import com.example.slist.model.Product;

import java.util.List;

public class StoreParentAdapter extends RecyclerView.Adapter<StoreParentAdapter.ViewHolder>{

    private Context context;
    private List<Category> categories;

    public StoreParentAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.store_child_layout, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Category category = categories.get(position);
        List<Product> products = category.getProducts();

        holder.categoryName.setText(category.getName());
        StoreProductAdapter storeProductAdapter = new StoreProductAdapter(context, products, position, category.getColorCode());
        holder.recyclerView.setAdapter(storeProductAdapter);

        holder.recyclerView.setNestedScrollingEnabled(false);

    }


    @Override
    public int getItemCount() {
        return categories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView categoryName;
        RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            categoryName = itemView.findViewById(R.id.rv_store_category_name);
            recyclerView = itemView.findViewById(R.id.rv_store_product_list);

        }

    }

}
