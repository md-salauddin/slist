package com.example.slist.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.slist.R;
import com.example.slist.interfacce.RvTodayList;
import com.example.slist.model.ShoppingList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TodayListAdapter extends RecyclerView.Adapter<TodayListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ShoppingList> shoppingLists;
    private RvTodayList rvTodayList;

    public TodayListAdapter(Context context, ArrayList<ShoppingList> shoppingLists, RvTodayList rvTodayList) {
        this.context = context;
        this.shoppingLists = shoppingLists;
        this.rvTodayList = rvTodayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.cs_today_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ShoppingList shoppingList = shoppingLists.get(position);
        TextView name, shopListHint;
        LinearLayout colorBar;

        name = holder.name;
        shopListHint = holder.shopListHint;
        colorBar = holder.llColorBar;

        name.setText(shoppingList.getName());
        shopListHint.setText(shoppingList.getShopHint());
        colorBar.setBackgroundColor(Color.parseColor(randomColor()));

    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name, shopListHint;
        LinearLayout llColorBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.cs_today_list_name_txt);
            shopListHint = itemView.findViewById(R.id.cs_today_list_hint_txt);
            llColorBar = itemView.findViewById(R.id.cs_today_bar_color);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            rvTodayList.onItemClick(getAdapterPosition());
        }
    }


    public String randomColor () {
        List<String> color = new ArrayList<>();

        color.add("#ff7979");
        color.add("#2cb1e1");
        color.add("#c182e0");
        color.add("#92c500");
        color.add("#f83a3a");

        Random rand = new Random();
        int x = rand.nextInt(4);

        return  color.get(x);

    }

}
